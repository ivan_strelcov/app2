import { Component } from '@angular/core';
const template = require('./header.html');
@Component({
  selector: 'header',
  template
})
export class HeaderComponent {};
